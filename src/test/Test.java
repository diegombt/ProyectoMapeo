/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import edu.mapeo.persistencia.conf.HibernateUtil;
import edu.mapeo.persistencia.entity.Candidatura;
import edu.mapeo.persistencia.entity.Departamento;
import edu.mapeo.persistencia.entity.Persona;
import edu.mapeo.persistencia.entity.Voto;
import java.util.Date;
import org.hibernate.Session;

/**
 *
 * @author USER
 */
public class Test {

    public void agregarDepartamento(String nombre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Departamento departamento = new Departamento(nombre);
        session.save(departamento);
        session.getTransaction().commit();
        session.close();
    }

    public void agregarPersona(String nombre, String tipoDocumento, int numeroDoc, boolean habilitado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Persona persona = new Persona(nombre, tipoDocumento, numeroDoc, habilitado);
        session.save(persona);
        session.getTransaction().commit();
        session.close();
    }

    public void registrarCandidatura(int idPresidente, int idVicepresidente) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        //////////////////////////////
        Persona presidente = (Persona) session.get(Persona.class, idPresidente);
        Persona vicepresidente = (Persona) session.get(Persona.class, idVicepresidente);
        Candidatura candidatura = new Candidatura(presidente, vicepresidente, new Date());
        //////////////////////////////
        session.save(candidatura);
        session.getTransaction().commit();
        session.close();
    }

    public void registrarVoto(int idCandidatura, int idDepartamento, int idPersona) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        //////////////////////////////
        Candidatura candidatura = (Candidatura) session.get(Candidatura.class, idCandidatura);
        Departamento departamento = (Departamento) session.get(Departamento.class, idDepartamento);
        Persona persona = (Persona) session.get(Persona.class, idPersona);
        Voto voto = new Voto(candidatura, departamento, persona);
        //////////////////////////////
        session.save(voto);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Test t = new Test();
//        t.agregarDepartamento("Nariño");
//        t.agregarPersona("Angela Bernal", "C", 1049625185, true);
//        t.agregarPersona("Henry Campos", "C", 1049625188, true);
//        t.registrarCandidatura(3, 4);
//        t.registrarVoto(1, 4, 6);
    }

}
