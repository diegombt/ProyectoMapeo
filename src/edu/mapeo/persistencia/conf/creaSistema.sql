SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `votacion` ;
CREATE SCHEMA IF NOT EXISTS `votacion` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `votacion` ;

-- -----------------------------------------------------
-- Table `votacion`.`Persona`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `votacion`.`Persona` ;

CREATE  TABLE IF NOT EXISTS `votacion`.`Persona` (
  `id_persona` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  `tipo_documento` VARCHAR(1) NOT NULL ,
  `numero_doc` INT NOT NULL ,
  `habilitado` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`id_persona`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `votacion`.`Candidatura`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `votacion`.`Candidatura` ;

CREATE  TABLE IF NOT EXISTS `votacion`.`Candidatura` (
  `id_candidatura` INT NOT NULL AUTO_INCREMENT ,
  `id_presidente` INT NOT NULL ,
  `id_vicepresidente` INT NOT NULL ,
  `periodo_electoral` DATE NOT NULL ,
  PRIMARY KEY (`id_candidatura`) ,
  INDEX `fk_Candidatura_Persona1_idx` (`id_presidente` ASC) ,
  INDEX `fk_Candidatura_Persona2_idx` (`id_vicepresidente` ASC) ,
  CONSTRAINT `fk_Candidatura_Persona1`
    FOREIGN KEY (`id_presidente` )
    REFERENCES `votacion`.`Persona` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Candidatura_Persona2`
    FOREIGN KEY (`id_vicepresidente` )
    REFERENCES `votacion`.`Persona` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `votacion`.`Departamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `votacion`.`Departamento` ;

CREATE  TABLE IF NOT EXISTS `votacion`.`Departamento` (
  `id_departamento` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_departamento`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `votacion`.`Voto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `votacion`.`Voto` ;

CREATE  TABLE IF NOT EXISTS `votacion`.`Voto` (
  `id_voto` INT NOT NULL AUTO_INCREMENT ,
  `id_persona` INT NOT NULL ,
  `id_candidatura` INT NOT NULL ,
  `id_departamento` INT NOT NULL ,
  PRIMARY KEY (`id_voto`) ,
  INDEX `fk_Voto_Persona1_idx` (`id_persona` ASC) ,
  INDEX `fk_Voto_Candidatura1_idx` (`id_candidatura` ASC) ,
  INDEX `fk_Voto_Departamento1_idx` (`id_departamento` ASC) ,
  CONSTRAINT `fk_Voto_Persona1`
    FOREIGN KEY (`id_persona` )
    REFERENCES `votacion`.`Persona` (`id_persona` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Voto_Candidatura1`
    FOREIGN KEY (`id_candidatura` )
    REFERENCES `votacion`.`Candidatura` (`id_candidatura` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Voto_Departamento1`
    FOREIGN KEY (`id_departamento` )
    REFERENCES `votacion`.`Departamento` (`id_departamento` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `votacion` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
