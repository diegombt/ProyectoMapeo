package edu.mapeo.persistencia.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Persona
 */
public class Persona implements java.io.Serializable {

    private Integer idPersona;
    private String nombre;
    private String tipoDocumento;
    private int numeroDoc;
    private boolean habilitado;
    private Set votos = new HashSet(0);
    private Set candidaturasPresidente = new HashSet(0);
    private Set candidaturasVicepresidente = new HashSet(0);

    public Persona() {
    }

    public Persona(String nombre, String tipoDocumento, int numeroDoc, boolean habilitado) {
        this.nombre = nombre;
        this.tipoDocumento = tipoDocumento;
        this.numeroDoc = numeroDoc;
        this.habilitado = habilitado;
    }

    public Integer getIdPersona() {
        return this.idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getNumeroDoc() {
        return this.numeroDoc;
    }

    public void setNumeroDoc(int numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public boolean isHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Set getCandidaturasPresidente() {
        return candidaturasPresidente;
    }

    public void setCandidaturasPresidente(Set candidaturasPresidente) {
        this.candidaturasPresidente = candidaturasPresidente;
    }

    public Set getCandidaturasVicepresidente() {
        return candidaturasVicepresidente;
    }

    public void setCandidaturasVicepresidente(Set candidaturasVicepresidente) {
        this.candidaturasVicepresidente = candidaturasVicepresidente;
    }

    public Set getVotos() {
        return votos;
    }

    public void setVotos(Set votos) {
        this.votos = votos;
    }

}
